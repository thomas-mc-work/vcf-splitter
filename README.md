# VCF Splitter

Bash script to separate a single vCard containing multiple contacts into multiple vCard files each with its own contact.

## Installation

    wget -P ~/bin/ "https://codeberg.org/thomas-mc-work/vcf-splitter/raw/branch/master/vcf-splitter.sh"
    chmod +x ~/bin/vcf-splitter.sh

## Usage

    $ ./vcf-split.sh FN example.vcf 
    outfile: 'linus torvalds.vcf'
    outfile: 'steve irwin.vcf'
    outfile: 'jacques-yves cousteau.vcf'

Output files are written to a subfolder based on the VCF file name.

Explanation: the second parameter (`FN`) is the name of the field whose value should be used for the file name. You can choose any other field, e.g. `UID`

## Credits

This script is a derivate of this one here:

https://gist.github.com/fabacab/531019dd8a1816fc5968ebb4db3b298b

Thanks [@fabacab](https://github.com/fabacab)!
