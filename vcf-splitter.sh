#!/usr/bin/env bash
#
# Bash script to separate a single vCard containing multiple contacts
# into multiple vCard files each with its own contact.
#
# Usage:
#
#     ./contact-split.sh <GUID_FIELD> <INPUT_FILE.vcf>
#
# where INPUT_FILE.vcf is, of course, the monolithic vCard  file.
# Output files are written to a subfolder based on the VCF file name.

main () {
    local card_lines
    local card_guid
    declare -a card_lines
    
    target_folder="${2%.*}"
    mkdir -p "$target_folder"

    while read line; do
        # Append each line to the end of this array.
        card_lines=("${card_lines[@]}" "$line")

        # Get the contact's GUID, this becomes the file name.
        if [[ "$line" =~ ^$1: ]]; then
            card_guid=$(echo -n "$line" | cut -d ':' -f 2)
        fi

        # This is the end of the card, so output it.
        if [[ "$line" =~ ^END:VCARD ]]; then
            if [ -z "$card_guid" ]; then
                echo "=> rejecting entry due to missing GUID:"
                printf "%s\n" "${card_lines[@]}"
            else
                outfile_name=$(echo -n "$card_guid" | tr 'A-Z' 'a-z' | tr -d '\r').vcf
                echo "outfile: '${outfile_name}'"
                printf "%s\n" "${card_lines[@]}" > "${target_folder}/${outfile_name}"
                unset -v card_lines
            fi
        fi
    done < "$2"
}

guid_field=${1?"param 1: GUID field name (e.g. UID, FN)"}
input_file=${2?"param 2: input file path"}

main "$guid_field" "$input_file"
